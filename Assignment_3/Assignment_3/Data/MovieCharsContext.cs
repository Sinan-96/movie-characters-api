﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Assignment_3.DatabaseModel;

    public class MovieCharsContext : DbContext
    {
        public MovieCharsContext (DbContextOptions<MovieCharsContext> options)
            : base(options)
        {
        }

        public DbSet<Assignment_3.DatabaseModel.Movie> Movie { get; set; }
    }
