﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment_3.DatabaseModel;
using System.Text;
using System.IO;

namespace Assignment_3
{
    public class MovieClassContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("" +
                $"Data Source={GetDataSource()};" +
                "Initial Catalog=MovieCharactersCodeFirstDB;" +
                "Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>()
                .HasData(new Movie
                {
                    Id = 1,
                    Title = "Dune",
                    Genre = "Drama",
                    ReleaseYear = 1984,
                    Director = "Ridley Scott",
                    Picture = "https://sfanytime-images-prod.secure.footprint.net/COVERM/COVERM_fa89f1f1-6257-4dd6-afab-d9a945796efe_01.jpg?w=375&fm=pjpg&s=b20cbda1a18d2edfc3bfb300b4b89ae7",
                    Trailer = "https://www.youtube.com/watch?v=KwPTIEWTYEI"

                }) ;

            modelBuilder.Entity<Movie>()
               .HasData(new Movie
               {
                   Id = 2,
                   Title = "Star wars : A new hope",
                   Genre = "Action,Adventure,Fantasy",
                   ReleaseYear = 1977,
                   Director = "George Lucas",
                   Picture = "https://www.imdb.com/title/tt0076759/mediaviewer/rm3263717120/",
                   Trailer = "https://www.youtube.com/watch?v=1g3_CFmnU7k"

               });

            modelBuilder.Entity<Movie>()
              .HasData(new Movie
              {
                  Id = 3,
                  Title = "Star wars : The empire strikes back",
                  Genre = "Action,Adventure,Fantasy",
                  ReleaseYear = 1980,
                  Director = "George Lucas",
                  Picture = "https://www.imdb.com/title/tt0080684/mediaviewer/rm3114097664/",
                  Trailer = "https://www.youtube.com/watch?v=JNwNXF9Y6kY"

              });

            modelBuilder.Entity<Character>()
             .HasData(new Character
             {
                 Id = 1,
                 FullName = "Luke Skywalker",
                 Alias = "Luki boy",
                 Gender = "Male",
                 Picture = "https://static.wikia.nocookie.net/starwars/images/b/bb/Luke_Skywalker.jpg/revision/latest/scale-to-width-down/250?cb=20080426161603&path-prefix=no"

             });

            modelBuilder.Entity<Character>()
             .HasData(new Character
             {
                 Id = 2,
                 FullName = "Darth vader",
                 Alias = "#1 dad",
                 Gender = "Male",
                 Picture = "https://cine.no/wp-content/uploads/2020/11/Darth-Vader.jpeg"
             });

            modelBuilder.Entity<Franchise>()
             .HasData(new Franchise
             {
                 Id = 1,
                 Name = "Star wars",
                 Description = "Intergalactic soap opera"


             });
        }

        private string GetDataSource()
        {
            return System.IO.File.ReadAllText("serverName.txt");
        }
    }
}