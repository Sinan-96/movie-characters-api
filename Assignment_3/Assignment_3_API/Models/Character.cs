﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3_API.DatabaseModel
{
    public class Character
    {
        [Required]
        public int CharacterId { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public IEnumerable<Movie> Movies { get; set; }
    }
}
