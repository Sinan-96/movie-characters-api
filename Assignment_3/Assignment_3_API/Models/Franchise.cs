﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3_API.DatabaseModel
{
    public class Franchise
    {
        [Required]
        public int Id { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<Movie> Movies;
    }
}
