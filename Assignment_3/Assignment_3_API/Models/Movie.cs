﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Assignment_3_API.DatabaseModel
{
    public class Movie
    {
        [Required]
        public int MovieId { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [Column(TypeName = "nvarchar(50)")]
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public IEnumerable<Character> Characters { get; set; }
        public Franchise franchise { get; set; }
    }
}
