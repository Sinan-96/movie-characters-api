﻿using Assignment_3_API.DatabaseModel;
using Assignment_3_API.Models.DTOs.Character;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3_API.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(adto => adto.movieIDs, opt => opt
                .MapFrom(a => a.Movies.Select(m => m.MovieId).ToList()))
                .ReverseMap();

            CreateMap<Character, CharacterCreateDTO>()
                .ForMember(adto => adto.movieIDs, opt => opt
                .MapFrom(a => a.Movies.Select(m => m.MovieId).ToList()))
                .ReverseMap();

            CreateMap<Character, CharacterEditDTO>()
                .ForMember(adto => adto.movieIDs, opt => opt
                .MapFrom(a => a.Movies.Select(m => m.MovieId).ToList()))
                .ReverseMap();
        }
    }
}
