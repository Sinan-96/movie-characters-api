﻿using Assignment_3_API.DatabaseModel;
using Assignment_3_API.Models.DTOs.Franchise;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3_API.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(adto => adto.movieIDs, opt => opt
                .MapFrom(a => a.Movies.Select(m => m.MovieId).ToList()))
                .ReverseMap();

            CreateMap<Franchise, FranchiseEditDTO>()
                .ForMember(adto => adto.movieIDs, opt => opt
                .MapFrom(a => a.Movies.Select(m => m.MovieId).ToList()))
                .ReverseMap();

            CreateMap<Franchise, FranchiseCreateDTO>()
                .ForMember(adto => adto.movieIDs, opt => opt
                .MapFrom(a => a.Movies.Select(m => m.MovieId).ToList()))
                .ReverseMap();
        }
    }
}
