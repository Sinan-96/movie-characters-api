﻿using Assignment_3_API.DatabaseModel;
using Assignment_3_API.Models.DTOs;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3_API.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(adto => adto.franchiseId, opt => opt
                .MapFrom(a => a.franchise.Id))
                .ForMember(adto => adto.characterIDs, opt => opt
                .MapFrom(a => a.Characters.Select(m => m.CharacterId).ToList()))
                .ReverseMap();

            CreateMap<Movie, MovieEditDTO>()
                .ForMember(adto => adto.franchiseId, opt => opt
                .MapFrom(a => a.franchise.Id))
                .ForMember(adto => adto.characterIDs, opt => opt
                .MapFrom(a => a.Characters.Select(m => m.CharacterId).ToList()))
                .ReverseMap();
            
            CreateMap<Movie, MovieCreateDTO>()
                .ForMember(adto => adto.franchiseId, opt => opt
                .MapFrom(a => a.franchise.Id))
                .ForMember(adto => adto.characterIDs, opt => opt
                .MapFrom(a => a.Characters.Select(m => m.CharacterId).ToList()))
                .ReverseMap();
        }
    }
}
