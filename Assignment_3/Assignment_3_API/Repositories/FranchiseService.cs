﻿using Assignment_3_API.DatabaseModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3_API.Repositories
{
    public class FranchiseService : IService<Franchise>
    {
        public readonly MovieClassContext _context;

        public FranchiseService(MovieClassContext context)
        {
            _context = context;
        }

        public async Task<Franchise> AddAsync(Franchise entity)
        {
            _context.Franchises.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            var movie = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(movie);
            await _context.SaveChangesAsync();

        }

        public bool EntityExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        public async Task<IEnumerable<Franchise>> GetAllAsync()
        {

            return await _context.Franchises
                .ToListAsync();

        }

        public async Task<Franchise> GetByIdAsync(int Id)
        {
            return await _context.Franchises.FindAsync(Id);
        }

        public async Task UpdateAsync(Franchise entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public async Task<Franchise> GetFranchiseMovies(int id)
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == id)
                .FirstAsync();
        }

        public async Task<Movie> GetMovieById(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        public async Task<Movie> GetMovieCharactersById(int id)
        {
            Movie m = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.MovieId == id)
                .FirstAsync();
            return m;
        }
    }
}

