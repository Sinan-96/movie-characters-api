﻿using Assignment_3_API.DatabaseModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3_API.Repositories
{
    public class MovieService : IService<Movie>
    {
        public readonly MovieClassContext _context;

        public MovieService(MovieClassContext context)
        {
            _context = context;
        }

        public async Task<Movie> AddAsync(Movie entity)
        {
            _context.Movies.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<Movie> GetMovieCharactersById(int id)
        {
            Movie m = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.MovieId == id)
                .FirstAsync();
            return m;
        }

        public async Task DeleteAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();

        }

        public bool EntityExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }

        public async Task<IEnumerable<Movie>> GetAllAsync()
        {

            return await _context.Movies
                .ToListAsync();

        }

        public async Task<Movie> GetByIdAsync(int Id)
        {
            return await _context.Movies.FindAsync(Id);
        }

        public async Task UpdateAsync(Movie entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }

        public async Task<Character> GetCharacterById(int id)
        {
            return await _context.Characters.FindAsync(id);
        }
    }
}
