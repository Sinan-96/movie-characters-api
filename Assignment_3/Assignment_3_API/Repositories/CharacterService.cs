﻿using Assignment_3_API.DatabaseModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3_API.Repositories
{
    public class CharacterService : IService<Character>
    {
        public readonly MovieClassContext _context;

        public CharacterService(MovieClassContext context)
        {
            _context = context;
        }

        public async Task<Character> AddAsync(Character entity)
        {
            _context.Characters.Add(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task DeleteAsync(int id)
        {
            var movie = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(movie);
            await _context.SaveChangesAsync();

        }

        public bool EntityExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }

        public async Task<IEnumerable<Character>> GetAllAsync()
        {

            return await _context.Characters
                .ToListAsync();

        }

        public async Task<Character> GetByIdAsync(int Id)
        {
            return await _context.Characters.FindAsync(Id);
        }

        public async Task UpdateAsync(Character entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            await _context.SaveChangesAsync();

        }
    }
}

