﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_3_API.DatabaseModel;
using AutoMapper;
using Assignment_3_API.Models.DTOs;
using Assignment_3_API.Repositories;

namespace Assignment_3_API
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieService _service;
        private readonly IMapper _mapper;

        public MoviesController(MovieService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all Movies
        /// </summary>
        /// <returns>List of Movies</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _service.GetAllAsync());
        }

        /// <summary>
        /// Gets a movie by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovieByID(int id)
        {
            var movie = await _service.GetByIdAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Gets characters in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of character Id's</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<int>>> GetMovieCharacters(int id)
        {
            var movie = await _service.GetMovieCharactersById(id);

            if (movie == null)
            {
                return NotFound();
            }

            if (movie.Characters == null)
                movie.Characters = new List<Character>();
            if (movie.Characters.ToList().Count <= 0)
                return BadRequest("Movie has no Characters!");

            List<int> movieCharacters = new();
            foreach (Character character in movie.Characters)
            {
                movieCharacters.Add(character.CharacterId);
            }
            return movieCharacters;
        }

        /// <summary>
        /// Modifies a movie by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoMovie"></param>
        /// <returns>NoContent</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if (!_service.EntityExists(id))
            {
                return NotFound();
            }
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _service.UpdateAsync(domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Updates charaters in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns>NoContent</returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!_service.EntityExists(id))
            {
                return NotFound();
            }

            //TODO: Create service for this
            Movie movieToUpdateCharacters = await _service.GetMovieCharactersById(id);

            List<Character> charactersList = new();
            foreach (int characterId in characters)
            {
                Character character = await _service.GetCharacterById(characterId);
                if (character == null)
                    return BadRequest("Character doesn't exist!");
                charactersList.Add(character);
            }
            movieToUpdateCharacters.Characters = charactersList;

            try
            {
                await _service.UpdateAsync(movieToUpdateCharacters);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();

        }

        /// <summary>
        /// Creates a movie in the database
        /// </summary>
        /// <param name="movieDTO"></param>
        /// <returns>NoContent</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostMovie(MovieCreateDTO movieDTO)
        {
            Movie domainMovie = _mapper.Map<Movie>(movieDTO);

            await _service.AddAsync(domainMovie);

            return CreatedAtAction("GeMovie",
                new { id = domainMovie.MovieId },
                _mapper.Map<MovieCreateDTO>(domainMovie));
        }

        /// <summary>
        /// Deletes a movie from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>NoContent</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_service.EntityExists(id))
            {
                return NotFound();
            }

            await _service.DeleteAsync(id);

            return NoContent();
        }
    }
}
