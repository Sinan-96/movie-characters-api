﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_3_API.DatabaseModel;
using AutoMapper;
using Assignment_3_API.Models.DTOs.Franchise;
using Assignment_3_API.Repositories;

namespace Assignment_3_API
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly FranchiseService _service;
        private readonly IMapper _mapper;

        public FranchisesController(FranchiseService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all Franchises in the database
        /// </summary>
        /// <returns>List of Franchises</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetAllFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _service.GetAllAsync());
        }

        /// <summary>
        /// Gets a Franchise by Id from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseByID(int id)
        {
            var franchise = await _service.GetByIdAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Gets Movies in a Franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of Movie Id's</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<int>>> GetFranchiseMovies(int id)
        {
            var franchise = await _service.GetFranchiseMovies(id);

            if (franchise == null)
            {
                return NotFound();
            }

            if (franchise == null)
                franchise.Movies = new List<Movie>();
            if (franchise.Movies.ToList().Count <= 0)
                return BadRequest("Franchise has no movies!");

            List<int> franchiseMovies = new();
            foreach (Movie movie in franchise.Movies)
            {
                franchiseMovies.Add(movie.MovieId);
            }
            return franchiseMovies;
        }

        /// <summary>
        /// Gets characters in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of character Id's</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<int>>> GetFranchiseCharacters(int id)
        {
            var franchise = await _service.GetFranchiseMovies(id);

            if (franchise == null)
            {
                return NotFound();
            }

            if (franchise == null)
                franchise.Movies = new List<Movie>();
            if (franchise.Movies.ToList().Count <= 0)
                return BadRequest("Franchise has no movies!");
            //uaaah! double foreach is probably not optimal here... Too many calls to _context via _service. ...but it's friday afternoon......
            List<int> franchiseCharacters = new();
            foreach (Movie movie in franchise.Movies)
            {
                var mov = await _service.GetMovieCharactersById(movie.MovieId);
                foreach (Character character in mov.Characters)
                {
                    franchiseCharacters.Add(character.CharacterId);
                }
            }
            return franchiseCharacters;
        }

        /// <summary>
        /// Modifies a franchise by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoFranchise"></param>
        /// <returns>NoContent</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }

            if (!_service.EntityExists(id))
            {
                return NotFound();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _service.UpdateAsync(domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Updates movies in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characters"></param>
        /// <returns>NoContent</returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            if (!_service.EntityExists(id))
            {
                return NotFound();
            }

            Franchise franchiseToUpdateMovies = await _service.GetFranchiseMovies(id);

            List<Movie> moviesList = new();
            foreach (int movieId in movies)
            {
                Movie movie = await _service.GetMovieById(id);
                if (movie == null)
                    return BadRequest("Movie doesn't exist!");
                moviesList.Add(movie);
            }
            franchiseToUpdateMovies.Movies = moviesList;

            try
            {
                await _service.UpdateAsync(franchiseToUpdateMovies);
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();

        }

        /// <summary>
        /// Creates a franchise in the database
        /// </summary>
        /// <param name="dtoFranchise"></param>
        /// <returns>CreatedAtAction</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            await _service.AddAsync(domainFranchise);

            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.Id },
                _mapper.Map<FranchiseCreateDTO>(domainFranchise));
        }

        /// <summary>
        /// Deletes a franchise from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>NoContent</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_service.EntityExists(id))
            {
                return NotFound();
            }

            await _service.DeleteAsync(id);

            return NoContent();
        }
    }
}
