﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_3_API.DatabaseModel;
using AutoMapper;
using Assignment_3_API.Models.DTOs.Character;
using Assignment_3_API.Repositories;

namespace Assignment_3_API
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly CharacterService _service;
        private readonly IMapper _mapper;

        public CharactersController(CharacterService service,IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all characters from database
        /// </summary>
        /// <returns>List of characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>( await _service.GetAllAsync());
        }

        /// <summary>
        /// Gets a character by Id from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>character</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _service.GetByIdAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Modifies a character by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCharacter"></param>
        /// <returns>NoContent</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }

            if (!_service.EntityExists(id))
            {
                return NotFound();
            }
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _service.UpdateAsync(domainCharacter);

            return NoContent();
        }

        /// <summary>
        /// Creates a new character in the database
        /// </summary>
        /// <param name="dtoCharacter"></param>
        /// <returns>CretedAtAction</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            
            await _service.AddAsync(domainCharacter);
            
            return CreatedAtAction("GetCharacter",
                new { id = domainCharacter.CharacterId },
                _mapper.Map<CharacterCreateDTO>(domainCharacter));
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_service.EntityExists(id))
            {
                return NotFound();
            }

            await _service.DeleteAsync(id);

            return NoContent();
        }
    }
}
