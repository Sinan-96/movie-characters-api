# Movie characters API
A simple, RESTful API to access the Movie-Characters Database.

## Full CRUD capability
Create, Read, Update and Delete from Franchises, Movies and Characters.
Endpoints have DTOs for easier and simpler access.
![Characters endpoints](https://gitlab.com/Sinan-96/movie-characters-api/-/raw/main/Assignment_3/Pictures/characters_api.png)

## Known Issues
- Seeding Franchise data
- Swagger documentation not complete (especially response codes)
